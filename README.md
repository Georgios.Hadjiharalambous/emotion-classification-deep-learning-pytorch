We combined multiple datasets of relatively low resolution images of faces. 
Through deep learning, using transfer learning with two different models, we tried to classify the emotions on these images.
Furthemore, through the process, we discovered the problems and why we are not able to achieve high accuracy.
For the optimization of the parameters we used Bayesian optimization with the help of [https://ax.dev/] (AX)